//
//  main.cpp
//  test
//
//  Created by André Venceslau on 29/11/2021.
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <iterator>
#include <set>

using namespace std;

typedef unsigned long long ull;
typedef long long ll;
typedef vector<ull> vec;


vec set_init_nosize(ull size){
	vec set(size, 0);
	for (ull i = 0; i < set.size(); i++) {
		set.at(i) = i;
	}
	return set;
}

/**
 * @brief Find the set representative of a value, compresses the path
 *
 * @param x value
 * @param set set
 * @return int
 */
ull set_findwcompression(ull x, vec &set){
//	ull root = x;
//	while (root != set.at(root)) {
//		root = set.at(root);
//	}
//	while (x != root) {
//		ull next = set.at(x);
//		set.at(x) = root;
//		x = next;
//	}
//	return root;
	return set.at(x);
}

void set_link(ull x, ull y, vec &set){
//	x = set_findwcompression(x, set);
//	y = set_findwcompression(y, set);
	if (x == y) {
		return;
	}
	set.at(y) = x;
}

/**
 * @brief prints a vec
 * 
 * @param input receives a vec
 */
void print(vec const &input) {
	for (ull i = 0; i < input.size(); i++) {
		if (i == 0) {
			cout << input.at(i);
			continue;
		}
		cout << ' ' << input.at(i);
	}
	cout << endl;
}

/**
 * @brief prints a vec
 *
 * @param input receives a vec
 */
void print_input(vec const &input) {
	for (ull i = 0; i < input.size(); i++) {
		if (i == 0) {
			cout << input.at(i);
			continue;
		}
		cout << ',' << input.at(i);
	}
	cout << endl;
}

/**
 * @brief calculates de maximum value in a vec
 * 
 * @param v1 is a vec containing values
 * @param v2 is a vec containing values
 * @return vec with common values
 */
set<ull> find_common(vec v1, vec v2){
	set<ull> save_v1;
	set<ull> common;
	for(ull val : v1)
		save_v1.insert(val);
	
	for(ull val : v2) {
		if (save_v1.count(val)) {
			common.insert(val);
		}
	}
	return common;
}

vec get_only_common(vec v, set<ull> common){
	vec only_commons;
	for(ull i = 0 ; i < v.size(); i++)
		if (common.count(v.at(i))) {
			only_commons.push_back(v.at(i));
		}
	return only_commons;
}

void preprocess_print(vec input, vec lens , vec cnts, vec red_sz){
	cout << "Input" << endl;
	print_input(input);
	cout << "Prepocessed lens and cnts" << endl;
	print(lens);
	print(cnts);
	cout << "Reduced Sz input" << endl;
	print(red_sz);
}

vec preprocess_for_num_LIS_len_cnt(vec input, vec &lens, vec &cnts){
	vec red_sz_input; //reduced size input
	ull count = 0;//current longest continuous sequence
	ull rep_neg_1st = 0; //Representative of a continuous sequence (greatest value)
	ull rep_neg_2nd = 0;
	ull rep_pos_1st = 0;
	ull rep_pos_2nd = 0;
	ull neg_count = 1;
	ull size = input.size();
	set<ull> already_inserted;
	for (ull i = 0; i < size; i++) {
		if (i < size-1 && input.at(i)+1 == input.at(i+1)){
			if (rep_pos_1st == 0)
				rep_pos_1st = input.at(i);
			rep_pos_2nd = input.at(i);
			count++;
		} else if (i < size -1 && input.at(i) == input.at(i+1)+1) {
			if(rep_neg_1st == 0)
				rep_neg_1st = input.at(i);
			rep_neg_2nd = input.at(i);
			neg_count++;
		} else {
			if (count > 0 && rep_neg_1st == 0) {
				red_sz_input.push_back(input.at(i));
				lens.push_back(count);
				cnts.push_back(neg_count);
			} else if (count > 0 && rep_neg_1st > 0) {
				red_sz_input.push_back(rep_neg_1st);
				lens.push_back(count);
				cnts.push_back(neg_count);
				
			} else {
				red_sz_input.push_back(input.at(i));
				lens.push_back(count);
				cnts.push_back(neg_count);
			}
			count = 0;
			neg_count = 1;
			rep_neg_1st = 0;
		}
	}
	preprocess_print(input, lens, cnts, red_sz_input);
	return red_sz_input;
}

vec preprocess_for_num_LIS_len_cnt_2(vec input, vec &lens, vec &cnts){
	vec red_sz_input; //reduced size input
	ull count = 0;//current longest continuous sequence
	ull rep_neg_1st = 0; //Representative of a continuous sequence (greatest value)
	ull rep_neg_2nd = 0;
	ull rep_pos_1st = 0;
	ull rep_pos_2nd = 0;
	ull neg_count = 1;
	ull size = input.size();
	set<ull> already_inserted;
	for (ull i = 0; i < size; i++) {
		if (i < size-1 && input.at(i)+1 == input.at(i+1)){
			if (rep_pos_1st == 0)
				rep_pos_1st = input.at(i);
			rep_pos_2nd = input.at(i);
			count++;
		} else if (i < size -1 && input.at(i) == input.at(i+1)+1) {
			if(rep_neg_1st == 0)
				rep_neg_1st = input.at(i);
			rep_neg_2nd = input.at(i);
			neg_count++;
		} else {
			if (count > 0 && rep_neg_1st == 0) {
				red_sz_input.push_back(rep_pos_1st);
				lens.push_back(0);
				cnts.push_back(1);
				red_sz_input.push_back(input.at(i));
				lens.push_back(count);
				cnts.push_back(neg_count);
			} else if (count > 0 && rep_neg_1st > 0) {
				red_sz_input.push_back(rep_neg_1st);
				lens.push_back(count);
				cnts.push_back(neg_count);
				
			} else {
				red_sz_input.push_back(input.at(i));
				lens.push_back(count);
				cnts.push_back(neg_count);
			}
			count = 0;
			neg_count = 1;
			rep_neg_1st = 0;
		}
	}
	preprocess_print(input, lens, cnts, red_sz_input);
	return red_sz_input;
}

vec preprocess_for_num_LIS(vec input, vec &lens, vec &cnts){
	vec red_sz_input; //reduced size input
	ull count = 0;//current longest continuous sequence
	ull size = input.size();
	set<ull> already_inserted;
	for (ull i = 0; i < size; i++) {
		if (i < size-1 && input.at(i)+1 == input.at(i+1)){
			count++;
		} else {
			if (count > 0) {
				red_sz_input.push_back(input.at(i));
				lens.push_back(count);
				cnts.push_back(1);
				count = 0;
			} else {
				red_sz_input.push_back(input.at(i));
				lens.push_back(0);
				cnts.push_back(1);
			}
		}
	}
	return red_sz_input;
}


/**
 * @brief counts the number of maximum lengths
 * 
 * @param cnts vec with the counts
 * @param lens vec with the lengths
 * @param max_len max lengths found
 * @return ull count of max lengths
 */
ull count_max_lens(vec cnts, vec lens, ull max_len){
	ull count = 0;
	for (ull i = 0; i < lens.size() ; i++){
		if (lens.at(i) == max_len) {
			count += cnts.at(i);
		}
	}
	return count;
}

/**
 * @brief finds the number of Longest Increasing Subsequences in O(N^2) time and O(N) space
 * 	we count how many subsequences end and each index and calculate max lengths
 * 	we can than iterate over the lengths and sum the counts for each index that has max length
 * 
 * @param values vec with values
 * @return vec the count and the length of the LIS
 */
vec find_number_of_LIS(vec values){						// 1,7,2,8,12,11,10
	ull count = 0;
	ull size = values.size();
	
	vec lengths(size, 1); //Length at each index
	vec counts(size, 1); //Count of how many subsequences end at a certain index
	ull max_len = 1;
	for (ull i = 1; i < size ; i++) { //i is upper bound index
		for (ull j = 0 ; j < i; j++) { //j is lower bound index
			//If is increasing
			if(values.at(i) > values.at(j)){
				/*
				 If the lower index length+1 is higher than the higher index length than we can be sure that
				 the length of the upper bound is at least equal to the lower index length + 1 and the number
				 of subsequences that end at the upper bound is at least equal equal to the lower bound
				*/
				if (lengths.at(j)+1 > lengths.at(i)) {
					lengths.at(i) = lengths.at(j) + 1;
					max_len = max(max_len, lengths.at(i));
					counts.at(i) = counts.at(j);
				/*
				 If the lower bound index length + 1 equals the upper bound index length than there are x new
				 subsequences ending at the upper bound index where x is the count of subsequences ending at
				 the lower bound index
				 */
				} else if (lengths.at(j) + 1 == lengths.at(i)) {
					counts.at(i) += counts.at(j);
				}
			}
		}
	}
	
	count = count_max_lens(counts, lengths, max_len); //Sum all the counts from the highest length subsequences
	vec res{ max_len, count };
	return res;
}

vec find_number_of_LIS_Optimized(vec input){
	ull count = 0;
	vec prepocessed_lens;
	vec prepocessed_cnts;
	vec values = preprocess_for_num_LIS_len_cnt(input, prepocessed_lens, prepocessed_cnts);
	ull size = values.size();
	ull max_len = 1;
	vec lengths(size, 1); //Length at each index
	vec counts(size, 1); //Count of how many subsequences end at a certain index

	for (ull i = 0; i < size ; i++) { //i is upper bound index
		for (ull j = 0 ; j < i; j++) { //j is lower bound index
			//If is increasing
			if(values.at(i) > values.at(j)){
				/*
				 If the lower index length+1 is higher than the higher index length than we can be sure that
				 the length of the upper bound is at least equal to the lower index length + 1 and the number
				 of subsequences that end at the upper bound is at least equal equal to the lower bound
				*/
				if (lengths.at(j) + 1 > lengths.at(i)) {
					lengths.at(i) = lengths.at(j) + 1;
					counts.at(i) = counts.at(j);
					max_len = max(max_len, lengths.at(i));
				/*
				 If the lower bound index length + 1 equals the upper bound index length than there are x new
				 subsequences ending at the upper bound index where x is the count of subsequences ending at
				 the lower bound index
				 */
				} else if (lengths.at(j) + 1 == lengths.at(i)) {
					counts.at(i) += counts.at(j);
				}
			}
		}
		lengths.at(i) += prepocessed_lens.at(i);
		counts.at(i) *= prepocessed_cnts.at(i);
		max_len = max(max_len, lengths.at(i));
	}
	print(lengths);
	count = count_max_lens(counts, lengths, max_len); //Sum all the counts from the highest length subsequences
	vec res{ max_len, count };
	return res;
}

/**
 * @brief Finds the Longest Common Increasing Subsquence in O(N^2) time
 * 		Similar to LIS but we only evaluate the numbers that are common between both vecs
 * 
 * @param values_1 Vector 1 refered to as V1
 * @param values_2 Vector 2 refered to as V2
 * @return vec with the max LCIS
 */
vec find_LCIS(vec values_1, vec values_2){
	/*
	 Keeps the greatest common length ending at each index
	 We start at 0 for the case where there are no overlapping values where the LCIS would be 0
	 */
	vec LCIS_lens(values_2.size(), 0);
	ull max_len = 0;
	
	
	for (ull i = 0; i < values_1.size(); i++) { //i iterates over the first set of values
		ull curr_high_len = 0; //Highest len seen in the inner lopp
		for (ull j  = 0; j < values_2.size() ; j++) { //j iterates over the second set of values
			/*
			 If V1[i] equals V2[j] then we are in the presence of a common subsequence
			 We then check if the current highest length + 1 is higher than the length at V2[j]
			 If it is then we have a new length maximum for a subsequence ending at V2[j]
			 */
			if (values_1.at(i) == values_2.at(j) && curr_high_len + 1 > LCIS_lens.at(j)) {
				LCIS_lens.at(j) = curr_high_len + 1;
				max_len = max(max_len, LCIS_lens.at(j));
			}

			/*
			 If V1[i] > V2[j] and the length[j] is greater than the current highest len than we have
			 to update the highest currest len to be equal to the highest seen len which is length[j]
			 */
			if (LCIS_lens.at(j) > curr_high_len && values_1.at(i) > values_2.at(j))
				curr_high_len = LCIS_lens.at(j);
		}
	}
	
	vec res{ max_len }; //Find the greatest len
	return res;
}

ull get_prob_info(vec &p1, vec &p2){
	ull prob = 0;
	
	string line;
	vector<vec> all_integers;
	while (getline(cin, line)) {
		istringstream var(line); 
		all_integers.push_back(
							   vec(
												istream_iterator<ull>(var),
												istream_iterator<ull>()
												)
							   );
	}
	
	prob = all_integers.at(0).at(0);
	if (prob != 1 && prob != 2)
		return 0;
	
	p1 = all_integers.at(1);
	
	if (prob == 2) {
		p2 = all_integers.at(2);
	}
	
	return prob;
}

int main(int argc, const char * argv[]) {
	// 6,8,9,7,9,14,15,17,15,20
	//8,13,15,16,19,16,13,14,19,20,25,22,24,27,31
	vec prob1_vec{8,13,15,16,19,16,13,14,19,20,25,22,24,27,31}; // {1,2,3,6,7} {1,2,3,5,7} {10,2,1,4,5,6,9,8,7,3} {5,9,7,8,13,10,7,10,15,16}
	vec prob2_vec;
	vec tmp;
	ull prob = 1;//get_prob_info(prob1_vec, prob2_vec);

	if (prob == 1) {
		print_input(prob1_vec);
		print(find_number_of_LIS(prob1_vec));
		print(find_number_of_LIS_Optimized(prob1_vec));
	} else if (prob == 2) {
		set<ull> common = find_common(prob1_vec, prob2_vec);
		print(find_LCIS(get_only_common(prob1_vec, common), get_only_common(prob2_vec, common)));
	} else {
		cout << "Invalid Problem" << endl;
	}
	return 0;
}

