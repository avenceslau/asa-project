//
//  main.cpp
//  test
//
//  Created by André Venceslau on 29/11/2021.
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <iterator>
#include <set>
#include <limits.h>

using namespace std;

typedef unsigned long long ull;
typedef long long ll;
typedef vector<ull> vec;

/**
 * @brief prints a vec
 * 
 * @param input receives a vec
 */
void print(vec const &input) {
	for (ull i = 0; i < input.size(); i++) {
		if (i == 0) {
			cout << input.at(i);
			continue;
		}
		cout << ' ' << input.at(i);
	}
	cout << endl;
}

/**
 * @brief creates a set with the common values between v1 and v2
 * 
 * @param v1 is a vec containing values
 * @param v2 is a vec containing values
 * @return set with common values
 */
set<ull> find_common(vec v1, vec v2){
	set<ull> save_v1;
	set<ull> common;
	for(ull val : v1)
		save_v1.insert(val);
	
	for(ull val : v2) {
		if (save_v1.count(val)) {
			common.insert(val);
		}
	}
	return common;
}

/**
 * @brief returns a new vector with only the values contained in the common set
 *
 * @param v is a vec containing values
 * @param common is a vec containing values
 * @return new vector with only the values contained in common
 */
vec get_only_common(vec v, set<ull> common){
	vec only_commons;
	for(ull i = 0 ; i < v.size(); i++)
		if (common.count(v.at(i))) {
			only_commons.push_back(v.at(i));
		}
	return only_commons;
}

/**
 * @brief counts the number of maximum lengths
 * 
 * @param cnts vec with the counts
 * @param lens vec with the lengths
 * @param max_len max lengths found
 * @return ull count of max lengths
 */
ull count_max_lens(vec cnts, vec lens, ull max_len){
	ull count = 0;
	for (ull i = 0; i < lens.size() ; i++){
		if (lens.at(i) == max_len) {
			count += cnts.at(i);
		}
	}
	return count;
}

/**
 * @brief removes values that do not need to be processed by LIS
 *		{10,10,10,1,2,3,1,1,1}
 *		We can ignore the greatest values at the beginning of the set and the smallest at the end
 *		We only need them if length equals zero in which case we only need to know how many we removed
 *		To add to the number of LIS
 *
 * @param input receives a vec
 * @return vec with the reduced size inputs
 */
vec remove_non_sense_vals(vec input, ull &removed){
	vec red_sz_ipt; //reduced size input
	ull greatest_num = 0;
	ull smallest_num = ULLONG_MAX;
	ull cnt = 1; //counts smallest numbers at the end
	removed = 0;
	ull size = input.size();
	for (ull i = 0; i < size; i++) {
		ull x = input.at(i);
		if (x == smallest_num)
			cnt++;
		else
			cnt = 1;
		smallest_num = min(smallest_num, x);
		greatest_num = max(greatest_num, x);
		
	}
	
	for (ull i = 0; i < size; i++) {
		if (i == 0 && input.at(i) == greatest_num) {
			while (i < size && input.at(i) == greatest_num) {
				i++;
				removed++;
			}
			if (i >= size) {
				break;
			}
		}
		if (i == size-cnt && input.at(i) == smallest_num) {
			removed += cnt;
			break;
		}
		red_sz_ipt.push_back(input.at(i));
	}
	return red_sz_ipt;
}

/**
 * @brief finds the number of Longest Increasing Subsequences in O(N^2) time and O(N) space
 * 	we count how many subsequences end and each index and calculate max lengths
 * 	we can than iterate over the lengths and sum the counts for each index that has max length
 *
 * @param removed number of values removed
 * @param values vec with values
 * @return vec the count and the length of the LIS
 */
vec find_number_of_LIS(vec values, ull removed){
	ull count = 0;
	ull size = values.size();
	vec lengths(size, 1); //Length at each index
	vec counts(size, 1); //Count of how many subsequences end at a certain index
	ull max_len = 1;
	for (ull i = 1; i < size ; i++) { //i is upper bound index
		for (ull j = 0 ; j < i; j++) { //j is lower bound index
			//If is increasing
			if(values.at(i) > values.at(j)){
				/*
				 If the lower index length+1 is higher than the higher index length than we can be sure that
				 the length of the upper bound is at least equal to the lower index length + 1 and the number
				 of subsequences that end at the upper bound is at least equal equal to the lower bound
				*/
				if (lengths.at(j)+1 > lengths.at(i)) {
					lengths.at(i) = lengths.at(j) + 1;
					max_len = max(max_len, lengths.at(i));
					counts.at(i) = counts.at(j);
				/*
				 If the lower bound index length + 1 equals the upper bound index length than there are x new
				 subsequences ending at the upper bound index where x is the count of subsequences ending at
				 the lower bound index
				 */
				} else if (lengths.at(j) + 1 == lengths.at(i)) {
					counts.at(i) += counts.at(j);
				}
			}
		}
	}
	
	count = count_max_lens(counts, lengths, max_len); //Sum all the counts from the highest length subsequences
	//if length is 1 than we have to add all numbers removed
	if (max_len == 1)
		count += removed;
	vec res{ max_len, count };
	return res;
}

/**
 * @brief Finds the Longest Common Increasing Subsquence in O(N^2) time
 * 		Similar to LIS but we only evaluate the numbers that are common between both vecs
 * 
 * @param values_1 Vector 1 refered to as V1
 * @param values_2 Vector 2 refered to as V2
 * @return vec with the max LCIS
 */
vec find_LCIS(vec values_1, vec values_2){
	/*
	 Keeps the greatest common length ending at each index
	 We start at 0 for the case where there are no overlapping values where the LCIS would be 0
	 */
	vec LCIS_lens(values_2.size(), 0);
	ull max_len = 0;
	
	for (ull i = 0; i < values_1.size(); i++) { //i iterates over the first set of values
		ull curr_high_len = 0; //Highest len seen in the inner lopp
		for (ull j  = 0; j < values_2.size() ; j++) { //j iterates over the second set of values
			/*
			 If V1[i] equals V2[j] then we are in the presence of a common subsequence
			 We then check if the current highest length + 1 is higher than the length at V2[j]
			 If it is then we have a new length maximum for a subsequence ending at V2[j]
			 */
			if (values_1.at(i) == values_2.at(j) && curr_high_len + 1 > LCIS_lens.at(j)) {
				LCIS_lens.at(j) = curr_high_len + 1;
				max_len = max(max_len, LCIS_lens.at(j));
			}

			/*
			 If V1[i] > V2[j] and the length[j] is greater than the current highest len than we have
			 to update the highest currest len to be equal to the highest seen len which is length[j]
			 */
			if (LCIS_lens.at(j) > curr_high_len && values_1.at(i) > values_2.at(j))
				curr_high_len = LCIS_lens.at(j);
		}
	}
	
	vec res{ max_len }; //Find the greatest len
	return res;
}

/**
 * @brief finds the problem number and inserts values into p1 and p2
 *
 *
 * @param p1 first set of values
 * @param p2 second set of values
 * @return ull with the problem number
 */
ull get_prob_info(vec &p1, vec &p2){
	ull prob = 0;
	
	string line;
	vector<vec> lines;
	/*
	 for each line read line has vector of ull
	 */
	while (getline(cin, line)) {
		istringstream var(line); 
		lines.push_back(
							   vec(
												istream_iterator<ull>(var),
												istream_iterator<ull>()
												)
							   );
	}
	//1st line has the problem number only
	prob = lines.at(0).at(0);
	if (prob != 1 && prob != 2)
		return 0;
	//there is always a 2nd line with the 1st set of values
	p1 = lines.at(1);
	//if its problem 2 than the 3rd and last line has the 2nd set of values
	if (prob == 2) {
		p2 = lines.at(2);
	}
	
	return prob;
}

int main(int argc, const char * argv[]) {
	vec prob1_vec;
	vec prob2_vec;
	ull prob = get_prob_info(prob1_vec, prob2_vec);
		
	if (prob == 1) {
		ull removed;
		print(find_number_of_LIS(remove_non_sense_vals(prob1_vec, removed), removed));
	} else if (prob == 2) {
		set<ull> common = find_common(prob1_vec, prob2_vec);
		print(find_LCIS(get_only_common(prob1_vec, common), get_only_common(prob2_vec, common)));
	} else {
		cout << "Invalid Problem" << endl;
	}
	return 0;
}
